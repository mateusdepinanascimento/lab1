---
title: "Empreendedorismo e Inovação na Engenharia (PMI3817) – (2 créditos-aula)"
date: 2023-09-18
draft: false
---

Estando dentro da Poli, e da própria USP como um todo, a gente sempre ouve que é um lugar de onde surgem várias Startups. Mesmo tendo tido contato com esse mundo de algumas formas (sim, assistindo Shark Thank), nunca tinha de fato aprendido o processo por trás do surgimento de uma, e foi o que me chamou atenção ao falar com uma pessoa que já tinha cursado a matéria: você realmente aprende a criar uma Startup! Depois acabei descobrindo e até começando a cursar outras matérias desse ramo mas pra mim “Empreendedorismo e Inovação na Engenharia” foi a melhor: não é uma carga horária alta e experiência com a matéria não foi um fardo ao longo do semestre, foi uma matéria leve com um assunto interessante e atual. O projeto da disciplina é passar pelo começo do processo de desenvolvimento de uma startup em um grupo de em média 5 pessoas, contando com um pequeno pedaço da aula de conteúdo mesmo e o resto com mentorias (muito legais por sinal) e uma palestra de um empreendedor com relação ao tema da aula. Tem entregas sim, mas é pouca coisa e tem um monte de monitor para ajudar. Deu pra ir bem e ainda surpreenderam os alunos com um monte de prêmios pras Startups bem colocadas no final.

Nota: 10 - Fernanda Quelho (Engenharia Mecatrônica)



(Texto retirado de Jornal O Politécnico)