---
title: "Responsabilidade Social e Empreendedorismo Social (EAD0724)  – (2 créditos-aula)"
date: 2023-09-18
draft: false
---

A matéria foi uma ótima surpresa! A profª é absoluta referência no assunto e combinava: apresentação de conceitos, estudos de caso e conversas com empreendedores de muito sucesso (todos ex-alunos dela).

Fica o aviso de que a matéria é dada num formato intensivo durante as férias (o que eu não sabia quando me matriculei rs), aí achei um pouco puxado mesmo pra assistir todas as aulas e completar as leituras no ritmo que sugerem. Mas mesmo sem conseguir terminar as leituras a tempo, dava pra acompanhar a aula. O trabalho final é relativamente tranquilo, mais de pesquisar e aplicar alguns dos conceitos estudados e (apesar de não ser coxa) não foi um bicho de 7 cabeças e tinha um prazo de um mês pra focar nisso e terminar.

Recomendo bastante a matéria se tem curiosidade no assunto e quiser conhecer, certamente é uma daquelas oportunidades de ter aula com a própria pessoa que “inventou” os conceitos que os outros estudam.

Nota 8,5 - Vinicius Lopez (Engenharia Elétrica).


(Texto retirado de Jornal O Politécnico)