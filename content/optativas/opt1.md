---
title: "Modelos em Atuária e Finanças (MAP 2225) – (2 créditos-aula e 2 créditos-trabalho) "
date: 2023-09-18
draft: false
---

O conteúdo é bem abrangente, uma boa matéria para quem quer começar a ter contato com assuntos do mercado financeiro e se aprimorar em alguma linguagem de programação. Apesar de ser útil saber um pouco de programação, a grande maioria das entregas podem ser feitas tranquilamente utilizando apenas o Excel ou Google Sheets. 

As aulas no ead eram bem tranquilas, o professor não cobrava presença e as aulas ficavam gravadas. Ao contrário do monólogo das aulas da Poli, as aulas do IME possuem bastante discussão, o que ajuda a querer assistir uma aula sexta feira à noite.

De início, o conteúdo é bem simples e vai se aprofundando,  abordando desde conceitos iniciais até modelos bem complexos. Todavia, por ser uma matéria introdutória e com público bem heterogêneo, o professor não puxa muito nas entregas porém se coloca à disposição dos alunos que querem se aprofundar, recomendando materiais para estudo e estando aberto a tirar dúvidas. 

Acho que as aulas de modelagem são as melhores, podendo ter um contato com assuntos bem interessantes e altamente aplicáveis. 

Se você estiver afim de uma matéria abrangente, com um professor interessado e com uma cobrança justa, você encontrou sua optativa para o próximo semestre! Recomendo muito.

Nota 8 - Roberto Araújo (Engenharia Civil)


(Texto retirado de Jornal O Politécnico)