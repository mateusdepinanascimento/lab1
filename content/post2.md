---
title: "Escrito pelo Professor Cláudio Possani"
date: 2023-09-27
---

# Russas 
*Criança, eu ia feliz ao parque de diversões,*  
*heroico subia na montanha russa*  
*e bravamente pensava: venci.*  
*Adulto, tento conviver com as paixões,*  
*a vida se oferece como roleta russa*  
*e tantas vezes, angustiado, penso: perdi.*