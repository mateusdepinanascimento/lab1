---
title: "Texto retirado da Gazeta de São Paulo"
date: 2023-09-27
---

# A greve na Poli

Na noite desta segunda (25), pouco mais de 250 alunos da Escola Politécnica se reuniram em Assembleia para deliberar sobre uma consulta formal que seria realizada nos dias seguintes. Plebiscitos a esse molde foram, historicamente, os mecanismos acionados para garantir consultas representativas dos seis mil alunos de engenharia. Entretanto, com a pressão de grevistas da FFLCH e do DCE (Diretório Central dos Estudantes), a decisão final favorável à greve foi tomada ali mesmo – o que gerou a revolta dos alunos que não estavam presentes. 

As aulas foram suspensas temporariamente pela diretoria, enquanto o Grêmio Politécnico buscou fazer a consulta prometida aos estudantes. "Infelizmente, táticas de intimidação como essa são comuns na USP — te garanto que não aconteceu apenas na Poli. Óbvio que a justificativa da greve é válida, é um problema que remonta a seis, sete anos. Mas com certeza poderia ter sido conduzida de maneira mais democrática.", disse Mateus Nascimento (20), aluno de engenharia mecatrônica.

Na Faculdade de Direito, a decisão também foi tomada em assembleia, com a presença de 630 estudantes. Segundo o Centro Acadêmico XI de Agosto, 606 alunos votaram a favor da paralisação. Na terça-feira (26), outros institutos também aderiram ao movimento, como a Farmácia-Bioquímica.